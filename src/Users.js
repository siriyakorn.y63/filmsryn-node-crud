import React, { useState, useEffect } from "react";
import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import Avatar from '@mui/material/Avatar';
import Divider from '@mui/material/Divider';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import { CardContent, Container, Typography } from '@mui/material';

export default function BasicGrid() {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    fetch("https://siriyakornsry-node-crud.herokuapp.com/course")
      .then(res => res.json())
      .then(
        (result) => {
          setUsers(result);
        }
      )
  }, [])

  return (
    <Container maxWidth="lg" sx={{ mt: 2 }}>
      <Grid container spacing={2}>
        {users.map(user => (
          <Grid item xs={12} md={4}>
        <Card>
            <CardMedia
                component="img"
                image={user.cover}
              />
              <CardHeader
                title={user.id + '.' +user.name}
              />
              <CardContent disableSpacing>
                <ListItem alignItems="flex-start">
                <ListItemAvatar>
                <Avatar src={user.avatar} aria-label="recipe">
                    </Avatar>
                </ListItemAvatar>
                <ListItemText
                primary= {user.fname+' '+user.lname}
                secondary={
                    <React.Fragment>
                    <Typography
                        sx={{ display: 'inline' }}
                        component="span"
                        variant="body2"
                        color="text.primary"
                    >
                    </Typography>
                    </React.Fragment>
                }
                />
            </ListItem>
                  <Divider variant="middle" />
                  <Typography>
                      <h2>
                        {user.price}
                      </h2>
                  </Typography>
              </CardContent>
            </Card>
          </Grid>
        ))}
      </Grid>
    </Container>
  );
}

